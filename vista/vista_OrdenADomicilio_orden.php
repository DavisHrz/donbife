<?php 

    $categorias = $producto->obtenerCategorias();
    $todosDetalles = $producto->obtenerDetalles();

    //$file = 'imagenes/categorias/Bebidas.png';
    //$resizedFile = 'imagenes/categorias/Bebidas2.png';
    //smart_resize_image($file , null, 170 , 170 , false , $resizedFile , false , false ,100 );

?>

<div class="orden-div container-fluid">
    <?php include_once("vista/vista_LineaTiempo.php"); ?>

    <div class="orden-items" >

    <?php
        for ($i=0; $i < count($categorias); $i++){ 
         
        ?> 
        <div class="item animate__bounceIn" onclick='abrirSubMenu(<?php echo json_encode( $producto->obtenerProductos($categorias[$i][0]) );?>, "<?php echo $categorias[$i][1]; ?>")' id="categoria<?php echo $categorias[$i][0];?>" >
            <div class="item-img" >
                <img src="<?php echo $categorias[$i][2]; ?>">
            </div>
            <div class="item-text">
                <h5> <?php echo $categorias[$i][1]; ?> </h5>
            </div>
        </div>
        <?php

        }
    ?>

    </div>

    <div class="orden-siguiente">
        <button type="submit"  >Siguiente</button>
    </div>

</div>

<div class="modal animate__animated" id="modal">
    <div class="modal-dialog">
        <section class="modal-content">
            <header class="modal-header">
                <img class="close-modal" aria-label="close modal" data-close id="orden-felchaBlanca" src="imagenes/flechaBlanca.png" alt="flecha">
                <h3 class="modal-title" >TITULO</h3>
            </header>
            <section class="modal-content-items">
                <!--
                <div class="modal-item " >
                    <div class="modal-item-img" >
                        <img src="imagenes/categorias/Quesos.png">
                    </div>
                    <div class="modal-item-text">
                        <h5> QUESOS </h5>
                    </div>
                </div>
                -->

            </section>
        </section>
    </div>
</div>

<div class="modal2 animate__animated" id="modal2">
    <div class="modal2-dialog">
        <section class="modal2-content">
            <header class="modal2-header">
                <img class="close-modal2" aria-label="close modal2" data-close2 id="orden-felchaBlanca2" src="imagenes/flechaBlanca.png" alt="flecha">
                <h3 class="modal2-title" >ESPECIFICACIONES</h3>
            </header>
            <section class="modal2-content-items">
                <!--
                <div class="modal-item " >
                    <div class="modal-item-img" >
                        <img src="imagenes/categorias/Quesos.png">
                    </div>
                    <div class="modal-item-text">
                        <h5> QUESOS </h5>
                    </div>
                </div>
                -->

            </section>
        </section>
    </div>
</div>

<script type="text/javascript">
    let closeEls = document.querySelectorAll("[data-close]");
    let closeEls2 = document.querySelectorAll("[data-close2]");
    let isVisible = "is-visible";
    let opacity = "orden-isOpacity";
    let opacity2 = "orden-isOpacity2";


    function abrirSubMenu(productos, titulo){
        console.log(productos);
        let modal = document.getElementById('modal');
        let tituloModal = document.querySelector('.modal-title');
        let fondo =  document.querySelector('.orden-div');
        let items = document.querySelectorAll('.item');

        /* HACER VISIBLE Y QUITAR Y AÑADIR ANIMACIONES */
        modal.classList.add(isVisible);
        modal.classList.remove('animate__bounceInLeft');
        modal.classList.remove('animate__bounceOut');
        modal.classList.add('animate__bounceIn');

        fondo.classList.add(opacity);

        tituloModal.innerHTML = titulo;

        items.forEach(element => {
            element.classList.add(opacity2);
        });

        /* AÑADIR ITEMS AL SUBMENU */
        let divItems = document.querySelector('.modal-content-items');
        productos.forEach(producto => {

            let divModalItem = document.createElement('div');
            divModalItem.classList.add('modal-item');
    
            let divModalItemImg = document.createElement('div');
            divModalItemImg.classList.add('modal-item-img');
    
            let modalItemImg = document.createElement('img');
            modalItemImg.src = producto[2];
            divModalItemImg.appendChild(modalItemImg);
    
            let divModalItemText = document.createElement('div');
            divModalItemText.classList.add('modal-item-text');
    
            let modalItemText = document.createElement('h5');
            modalItemText.innerHTML = producto[1];
            divModalItemText.appendChild(modalItemText);
    
            divModalItem.appendChild(divModalItemImg);
            divModalItem.appendChild(divModalItemText);
    
            divItems.appendChild(divModalItem);

            divModalItem.onclick = () => {
                siguienteSubMenu(producto[0]);
            }

        });
    }
    async function cerrarSubMenu(){
        if (document.querySelector(".modal.is-visible")) {
            document.querySelector(".modal.is-visible").classList.remove('animate__bounceIn');
            document.querySelector(".modal.is-visible").classList.remove('animate__backOutLeft');
            document.querySelector(".modal.is-visible").classList.add('animate__bounceOut');
            await new Promise(r => setTimeout(r, 600));
            document.querySelector(".modal.is-visible").classList.remove(isVisible);
            document.querySelector('.orden-div').classList.remove(opacity);
            document.querySelectorAll('.item').forEach(element => {
                element.classList.remove(opacity2);
            });

            document.querySelector('.modal-content-items').remove();
            let divItemsNew = document.createElement('section');
            divItemsNew.classList.add('modal-content-items');
            document.querySelector('.modal-content').appendChild(divItemsNew);
        }
    }

    async function siguienteSubMenu(idProducto){
        console.log(idProducto);
        let todosDetalles = <?php echo json_encode( $todosDetalles );?>;
        let detalles = [];
        todosDetalles.forEach(element => {
            if(element[0] == idProducto){
                detalles.push(element);
            }
        });
        console.log(detalles);

        if (document.querySelector(".modal.is-visible")) {
            document.querySelector(".modal.is-visible").classList.remove('animate__bounceIn');
            document.querySelector(".modal.is-visible").classList.remove('animate__bounceInLeft');
            document.querySelector(".modal.is-visible").classList.add('animate__bounceOutLeft');
            await new Promise(r => setTimeout(r, 600));
            document.querySelector(".modal.is-visible").classList.remove(isVisible);

            /* HACER VISIBLE EL SIG SUBMENU */
            document.getElementById('modal2').classList.add(isVisible);
            document.getElementById('modal2').classList.remove('animate__bounceOut');
            document.querySelector(".modal2").classList.remove('animate__backOutRight');
            document.getElementById('modal2').classList.add('animate__bounceInRight');
        }

    }
    async function cerrarSubMenu2(){
        if (document.querySelector(".modal2.is-visible")) {
            document.querySelector(".modal2.is-visible").classList.remove('animate__bounceInRight');
            document.querySelector(".modal2.is-visible").classList.remove('animate__backOutRight');
            document.querySelector(".modal2.is-visible").classList.add('animate__bounceOut');
            await new Promise(r => setTimeout(r, 600));
            document.querySelector(".modal2.is-visible").classList.remove(isVisible);
        }
    }

    for (let el of closeEls) {
        el.addEventListener("click", async function() {
            /*
            this.parentElement.parentElement.parentElement.parentElement.classList.remove('animate__bounceIn');
            this.parentElement.parentElement.parentElement.parentElement.classList.add('animate__bounceOut');
            await new Promise(r => setTimeout(r, 600));
            this.parentElement.parentElement.parentElement.parentElement.classList.remove(isVisible);
            document.querySelector('.orden-div').classList.remove(opacity);
            document.querySelectorAll('.item').forEach(element => {
                element.classList.remove(opacity2);
            });

            document.querySelector('.modal-content-items').remove();
            let divItemsNew = document.createElement('section');
            divItemsNew.classList.add('modal-content-items');
            document.querySelector('.modal-content').appendChild(divItemsNew);
            */
            cerrarSubMenu();
        });
    }
    for (let el2 of closeEls2){
        el2.addEventListener("click", async function(){
            /* CERRAR SUBMENU 2 */
            document.querySelector(".modal2.is-visible").classList.remove('animate__bounceInRight');
            document.querySelector(".modal2.is-visible").classList.remove('animate__bounceOut');
            document.querySelector(".modal2.is-visible").classList.add('animate__bounceOutRight');
            await new Promise(r => setTimeout(r, 600));
            document.querySelector(".modal2.is-visible").classList.remove(isVisible);
            document.querySelector(".modal2").classList.remove('animate__bounceOutRight');

            /* ABRIR SUBMENU 1 */
            document.querySelector(".modal").classList.add(isVisible);
            document.querySelector(".modal.is-visible").classList.remove('animate__bounceOutLeft');
            document.querySelector(".modal.is-visible").classList.remove('animate__bounceInRight');
            document.querySelector(".modal.is-visible").classList.add('animate__bounceInLeft');

        });
    }

    document.addEventListener("click", async e => {
        if (e.target == document.querySelector(".modal.is-visible")  ) {
            /*
            console.log(e.target);
            document.querySelector(".modal.is-visible").classList.remove('animate__bounceIn');
            document.querySelector(".modal.is-visible").classList.add('animate__bounceOut');
            await new Promise(r => setTimeout(r, 600));
            document.querySelector(".modal.is-visible").classList.remove(isVisible);
            document.querySelector('.orden-div').classList.remove(opacity);
            document.querySelectorAll('.item').forEach(element => {
                element.classList.remove(opacity2);
            });

            document.querySelector('.modal-content-items').remove();
            let divItemsNew = document.createElement('section');
            divItemsNew.classList.add('modal-content-items');
            document.querySelector('.modal-content').appendChild(divItemsNew);
            */
            cerrarSubMenu();
        }
        if(e.target == document.querySelector('.modal2.is-visible')){
            cerrarSubMenu2();
            document.querySelector('.orden-div').classList.remove(opacity);
            document.querySelectorAll('.item').forEach(element => {
                element.classList.remove(opacity2);
            });
        }
        
    });

    document.addEventListener("keyup", async e => {
    // if we press the ESC
        if (e.key == "Escape") {
            cerrarSubMenu();
            cerrarSubMenu2();
        }
        document.querySelector('.orden-div').classList.remove(opacity);
        document.querySelectorAll('.item').forEach(element => {
                element.classList.remove(opacity2);
        });
    });

</script>