<div class="fondo-div container-fluid datos">
    <?php include_once("vista/vista_LineaTiempo.php"); ?>

    <form class="formularioCliente" action="index.php?pagina=3&parte=2" method="POST" >

            <div class="datos-busqueda">
                <label>Buscar:</label>
                <select type="text" Class="Buscador container" id="controlBuscador" type="text" name="nombreBuscador" onchange="llenarDatos(value); " >
                    <option value="-1" >
                        seleccione un cliente
                    </option>
                    <?php foreach  ( $directorios as $persona ){ ?>
                        <option value="<?php echo $persona[0]; ?>">
                            <?php echo $persona[1]. " - ".$persona[3] ;  ?>
                        </option>
                    <?php } ?>
                </select>
                <button>Buscar</button>
            </div>
            <br />
        
        
            <div class="datos-cliente">
                <div class="datos-nombre">
                    <label>Nombre:</label>
                    <br />
                    <input class="nombre" type="text" name="nombre" autocomplete="off" required>
                </div>
                <div class="datos-telefono">
                    <label>Telefono:</label>
                    <br />
                    <input class="telefono" type="text" name="telefono" autocomplete="off" required>
                </div>
                <br> <br> 

                <div class="datos-colonia">
                    <label>Colonia:</label>
                    <br />
                    <input class="colonia" type="text" name="colonia" autocomplete="off">
                </div>
                <div class="datos-calle">
                    <label>Calle:</label>
                    <br />
                    <input class="calle" type="text" name="calle" autocomplete="off">
                </div>
                <br> <br> <br>
        
                <div class="datos-costo">
                    <label>Costo de envio:</label>
                    <input class="costo" type="number" name="costo" autocomplete="off" required>
                </div>
                <br> <br> <br>
        
                <div class="datos-referencia">
                    <label>Referencia:</label>
                    <br />
                    <textarea class="referencia" cols="30" rows="4" wrap="hard" name="referencia" autocomplete="off" required></textarea>
                </div>
                <br> <br> <br>
            </div>
    
            <div class="datos-siguiente">
                <button type="submit" name="datosCliente" id="datosCliente" >Siguiente</button>
            </div>
            <br> <br> <br>
    </form>
</div>

<script type="text/javascript">
    let inputBuscador = document.querySelectorAll('.Buscador')[0];
    let formularioCliente = document.querySelectorAll('.formularioCliente')[0];
    let inputNombre = document.querySelectorAll('.nombre')[0];
    let inputTelefono = document.querySelectorAll('.telefono')[0];
    let inputColonia = document.querySelectorAll('.colonia')[0];
    let inputCalle = document.querySelectorAll('.calle')[0];
    let inputCosto = document.querySelectorAll('.costo')[0];
    let inputReferencia = document.querySelectorAll('.referencia')[0];

    formularioCliente.onkeypress = function(e) {    
        if (e.keyCode == 13 && !(document.activeElement === inputReferencia) ) {
            if( document.activeElement === inputNombre ){
                inputTelefono.focus();
            }else if ( document.activeElement === inputTelefono ){
                inputColonia.focus();
            }else if ( document.activeElement === inputColonia ){
                inputCalle.focus();
            }else if ( document.activeElement === inputCalle ){
                inputCosto.focus();
            }else if ( document.activeElement === inputCosto ){
                inputReferencia.focus();
            }
            e.preventDefault();
        }else if(e.keyCode == 13 && document.activeElement === inputReferencia){
            let rows = inputReferencia.value.split('\n').length;
            if( rows == 4){
                formularioCliente.submit();
            }
            
        }
    }

    inputTelefono.onkeypress = function(e) {
        console.log(e);
        if(isNaN(this.value+String.fromCharCode(e.charCode)))
            return false;
    }

    inputBuscador.onkeypress = function(e) {
        console.log(e);
    }

    window.onload = function() {
        $(document).ready(function(){
            $('#controlBuscador').select2();
        });
    };

    function llenarDatos(id){
        if( id != -1 ){
            let directorios = <?php echo json_encode($directorios);?>;
            directorios.forEach(persona => {
                if(persona[0] == id){
                    let direccion = persona[2].split("-");
                    console.log(persona);
                    inputNombre.value = persona[1];
                    inputTelefono.value = persona[3];
                    inputColonia.value = direccion[0];
                    inputCalle.value = direccion[1];
                    inputCosto.value = persona[4];
                    inputReferencia.value = direccion[2];
                }
            });
        }else{
            inputNombre.value = "";
            inputTelefono.value = "";
            inputColonia.value = "";
            inputCalle.value = "";
            inputCosto.value = "";
            inputReferencia.value = "";
        }
    }


</script>