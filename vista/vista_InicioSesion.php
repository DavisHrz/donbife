<div class="inicioSesion">
    <div class="fondo-inicioSesion container-sm">
        <form class="form-container" action="index.php?op=1" method="post">
            <div class="titulo-inicioSesion">
                <label class="form-label">Inicio de sesión</label>
            </div>
    
            <div class="texto-inicioSesion">
                <label>Usuario</label>
                <div>
                    <input type="text" class="input-inicioSesion" id="usuario" name="usuario">
                </div>
            </div>
            <div class="texto-inicioSesion">
                <label>Contraseña</label>
                <div>
                    <input type="password" class="input-inicioSesion" id="password" name="password">
                </div>
            </div>
            
            <div class="boton-inicioSesion-div">
                <button type="submit">Ingresar</button>
            </div>
        </form>
    </div>

</div>