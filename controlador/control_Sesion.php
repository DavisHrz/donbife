<?php

// Aqui recibira los datos del formulario del login, en caso de que la contraseña este equivocada
// no se guardara nada y se redirigira al index
if( !empty($_GET["op"]) && isset($_GET["op"]) ){

    if( !empty($_POST["usuario"]) && isset($_POST["usuario"]) 
     && !empty($_POST["password"]) && isset($_POST["password"]) ){
         
         $user->USUARIO = $_POST["usuario"];
         $user->PASSWORD = $_POST["password"];
         if( $user->iniciarSesion() ){
             
            $_SESSION["ID"] = $user->NC;
            $_SESSION["USER"] = $user->USUARIO;
            $_SESSION["NIVEL"] = $user->NIVEL;
            $_SESSION["TIME"] = $_SERVER['REQUEST_TIME'];
            $_SESSION["TIMEOUT"] = 60*30; //min

         }
    }
    header('Location: index.php');
}

// Si no hay ninguna id guardada(por lo tanto no hay ninguna session iniciada)
if( (empty($_SESSION["ID"])) ){

    include_once("vista/vista_InicioSesion.php");

}else{

    include_once("controlador/control_Inicio.php");
    
}
?>