<?php
$time = $_SERVER['REQUEST_TIME'];

if( !empty($_SESSION["TIMEOUT"]) && isset($_SESSION["TIMEOUT"]) ){

    if( $_SESSION["TIMEOUT"] == 0){
        //La sesion nunca se cerrara
    }else{
        //La sesion se cerrara si paso mas de X minutos inactivo
        if (isset($_SESSION["TIME"]) && ($time - $_SESSION["TIME"]) >  $_SESSION["TIMEOUT"]) {
            session_unset();
            session_destroy();
            header('Location: index.php');
        }
    
    }

}

$_SESSION["TIME"] = $time;

?>