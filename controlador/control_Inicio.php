<?php
require_once("controlador/control_TiempoFuera.php");
require_once("vista/vista_Menu.php");

if( !empty($_GET["pagina"]) && isset($_GET["pagina"]) ){
    
    switch( $_GET["pagina"] ){
        case 1:
            include_once("vista/vista_Inicio.php");
            break;
        case 2:
            /*
            include_once("vista/vista_Orden.php");
            */
            break;
        case 3:
            include_once("controlador/control_OrdenADomicilio.php");
            break;
        case 90:
            session_destroy();
            header('Location: index.php');
            break;
        default:
            header('Location: index.php?pagina=1');
            //include_once("vista/vista_Inicio.php");
    }

    echo '<script> navSeleccionado('.$_GET["pagina"].') </script>';

}else{

    header('Location: index.php?pagina=1');
    //include_once("vista/vista_Inicio.php");

}

?>