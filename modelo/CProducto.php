<?php
    require_once("CConecta.php");
    class CProducto{
        var $NC;
        var $NOMBRE;
        var $CATEGORIA;
        var $IMAGEN;
        var $db;

        function __construct(){
            $con = new CConecta();
            $this->db = $con->conexion();
        }

        function obtenerCategorias(){
            $categorias = array();
            $querySELECT = 'SELECT * FROM tb_cate; ';
			if( $queryDB = mysqli_query($this->db, $querySELECT )){
                while ( $resultado = mysqli_fetch_assoc($queryDB) ){
                    $categoria = array();
                    array_push($categoria, $resultado["CATE_nc"]);
                    array_push($categoria, $resultado["CATE_nom"]);
                    array_push($categoria, $resultado["CATE_img"]);

                    array_push($categorias, $categoria);
                }
			}
	        return $categorias;
        }

        function obtenerProductos($idCategoria){
            $productos = array();
            $querySELECT = 'SELECT * FROM tb_pro WHERE PRO_Cat = "'.$idCategoria.'"; ';
			if( $queryDB = mysqli_query($this->db, $querySELECT )){
                while ( $resultado = mysqli_fetch_assoc($queryDB) ){
                    $producto = array();
                    array_push($producto, $resultado["PRO_Nc"]);
                    array_push($producto, $resultado["PRO_Nom"]);
                    array_push($producto, $resultado["PRO_Img"]);

                    array_push($productos, $producto);
                }
			}
	        return $productos;
        }

        function obtenerDetalles(){
            $detalles = array();
            $querySELECT = 'SELECT * FROM tb_prodt WHERE 1';
			if( $queryDB = mysqli_query($this->db, $querySELECT )){
                while ( $resultado = mysqli_fetch_assoc($queryDB) ){
                    $detalle = array();
                    array_push($detalle, $resultado["PRO_Nc"]);
                    array_push($detalle, $resultado["PRODT_Nc"]);
                    array_push($detalle, $resultado["PRODT_Nom"]);
                    array_push($detalle, $resultado["PRODT_Cos"]);

                    array_push($detalles, $detalle);
                }
			}
	        return $detalles;
        }

        function resize($width = NULL, $height = NULL, $targetFile, $originalFile) {
            $img = imagecreatefrompng($originalFile);
            /*if custom values for width and height are not set, 
             *Calculate from image original width and height
             */
            if($width != NULL && $height != NULL){
                $newWidth = $width;
                $newHeight = $height;
            }else{
                $imgWidth = imagesx($img);//get width of original image
                $imgHeight = imagesy($img);
                $newWidth = intval($imgWidth / 4);
                $newHeight = intval($imgHeight /4);
            }
            $newImage = imagecreatetruecolor($newWidth, $newHeight);
            imagealphablending($newImage, false);
            imagesavealpha($newImage,true);
            $transparency = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $newHeight, $newHeight, $transparency);
            imagecopyresampled($newImage, $img, 0, 0, 0, 0, $newWidth, $newHeight, $imgWidth, $imgHeight);
            imagepng($newImage,$targetFile);
        }



    }
?>