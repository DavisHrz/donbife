<?php
    require_once("CConecta.php");
    class CUsuario{
        var $NC;
        var $USUARIO;
        var $PASSWORD;
        var $NIVEL;
        var $db;

        function __construct(){
            $con = new CConecta();
            $this->db = $con->conexion();
        }

        function iniciarSesion(){
            $respuesta = false;
            $querySELECT = 'SELECT * FROM tb_usu WHERE USU_Usu="'.$this->USUARIO.'" AND USU_Con="'.md5($this->PASSWORD).'"; ';
			if($queryDB = mysqli_query($this->db, $querySELECT)){
                $resultado = mysqli_fetch_assoc($queryDB);
                $this->NC = $resultado["USU_Nc"];
                $this->USUARIO = $resultado["USU_Usu"];
			    $this->NIVEL = $resultado["USU_Niv"];
			    return true;
			}
	        return $respuesta;
        }

        function numeroDirectorios(){
            $respuesta = -1;
            $querySELECT = 'SELECT count(*) AS TOTAL FROM tb_dir;';
			if( $queryDB = mysqli_query($this->db, $querySELECT )){
                $resultado = mysqli_fetch_assoc($queryDB);
			    return $resultado["TOTAL"];
			}
	        return $respuesta;
        }

        function directorios(){
            $directorios = array();
            $querySELECT = 'SELECT * FROM tb_dir; ';
			if( $queryDB = mysqli_query($this->db, $querySELECT )){
                while ( $resultado = mysqli_fetch_assoc($queryDB) ){
                    $directorio = array();
                    array_push($directorio, $resultado["DIR_Nc"]);
                    array_push($directorio, $resultado["DIR_Nom"]);
                    array_push($directorio, $resultado["DIR_Dir"]);
                    array_push($directorio, $resultado["DIR_Num"]);
                    array_push($directorio, $resultado["DIR_Cos"]);

                    array_push($directorios, $directorio);
                }
			}
	        return $directorios;
        }

    }
?>