<?php
class CConecta{
   var $host;
   var $user;
   var $pw;
   var $nombreBD;

   function __construct(){
      $this->host= HOST;
      $this->user= USER;
      $this->pw= PASS;
      $this->nombreBD= DB;
   }

   function conexion(){
      if($db = mysqli_connect($this->host, $this->user, $this->pw)){
        if(mysqli_select_db($db, $this->nombreBD)){
            return $db;
        }else{
          die("Base de datos inexistente");
        }
      }else{
        die("No hay conexion");
      }
    }
  }
?>